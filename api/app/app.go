package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/BurntSushi/toml"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"

	"github.com/makhanu/vaspro-dlrs/api/app/producer"
)

// Person :
type Person struct {
	Name string `json:"Name"`
	Age  string `json:"Age"`
}

// Devconfig --
type Devconfig struct {
	Devlevel string
}

func (c *Devconfig) Read() {
	if _, err := toml.DecodeFile("/apps/go/vas-dlrs/configs/config.toml", &c); err != nil {
		//if _, err := toml.DecodeFile("/Users/lukemakhanu/go/src/github.com/makhanu/vaspro-dlrs/configs/config.toml", &c); err != nil {
		log.Fatal(err)
	}
}

// App - main app struct
type App struct {
	Router *mux.Router
	Viper  *viper.Viper
	lg     *lumberjack.Logger
	amqp   *amqp.Config
}

// Viperconfigs -- loads viper configs
func (a *App) Viperconfigs() {
	var devprod = Devconfig{}
	devprod.Read()

	a.Viper = viper.New()
	a.Viper.SetDefault("host", "localhost")

	var level = devprod.Devlevel
	if level == "development" {
		a.Viper.SetConfigName("development")
		log.Println("running on development configs")
	} else {
		a.Viper.SetConfigName("production")
		log.Println("running on production configs")
	}

	//a.Viper.SetConfigName("config")
	//a.Viper.AddConfigPath("/Users/lukemakhanu/go/src/github.com/makhanu/vaspro-dlrs/configs/")
	a.Viper.AddConfigPath("/apps/go/vas-dlrs/configs/")
	a.Viper.AddConfigPath(".")
	err := a.Viper.ReadInConfig()
	if err != nil {
		log.Println("config error should be logged ", err)
	}
	log.Println("viper initiated successfully ")
}

// Initialize
func (a *App) Initialize() {
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

// Loggerconfigs - initializes logging configs
func (a *App) Loggerconfigs() {
	log.SetOutput(&lumberjack.Logger{
		Filename:   a.Viper.GetString("system_logging.consumers_log_url"),
		MaxSize:    a.Viper.GetInt("system_logging.MaxSize"),
		MaxBackups: a.Viper.GetInt("system_logging.MaxBackups"),
		MaxAge:     a.Viper.GetInt("system_logging.MaxAge"),
		Compress:   a.Viper.GetBool("system_logging.Compress"),
	})
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)
	log.Println("microseconds added to prefix")
}

// Run - the main fun to run
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

// initializeRoutes --
func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/dlrs", a.delreports).Methods("POST")
}

func (a *App) delreports(w http.ResponseWriter, r *http.Request) {
	fmt.Println("*** in createPerssion func ***")
	contents, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("error on loading remote data", err)
	}
	fmt.Println("*** content being sent is ****", contents)

	var uri = a.Viper.GetString("rabbitmq.conn")
	var exchangeName = a.Viper.GetString("publishers.dlrs.ExchangeDeclare")
	var exchangeType = "direct"
	var routingKey = a.Viper.GetString("publishers.dlrs.routingKey")
	var reliable = true
	log.Println("uri ", uri)
	log.Println("exchangeName ", exchangeName)
	log.Println("exchangeType ", exchangeType)
	log.Println("routingKey ", routingKey)
	log.Println("reliable ", reliable)

	if err := producer.Publish(uri, exchangeName, exchangeType, routingKey, contents, reliable); err != nil {
		log.Fatalf("%s", err)
	} else {
		log.Printf("createPermission data published %dB OK", contents)
	}
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
