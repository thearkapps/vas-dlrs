// main.go
package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"

	"github.com/makhanu/vaspro-dlrs/api/app"
)

func main() {
	a := app.App{}
	a.Viperconfigs()
	a.Loggerconfigs()
	a.Initialize()
	fmt.Println("server running on port :", a.Viper.GetString("system_default.port"))
	a.Run(a.Viper.GetString("system_default.port"))
}
