package common

import (
	"log"

	"github.com/BurntSushi/toml"
	"github.com/spf13/viper"
)

// Devconfig --
type Devconfig struct {
	Devlevel string
}

func (c *Devconfig) Read() {
	if _, err := toml.DecodeFile("/apps/go/vas-dlrs/configs/config.toml", &c); err != nil {
		//if _, err := toml.DecodeFile("/Users/lukemakhanu/go/src/github.com/makhanu/vaspro-dlrs/configs/config.toml", &c); err != nil {
		log.Fatal(err)
	}
}

// Configutils -- initialize configs
func Configutils() {
	viper.SetDefault("host", "localhost")
	var devprod = Devconfig{}
	devprod.Read()

	var level = devprod.Devlevel
	if level == "development" {
		viper.SetConfigName("development")
		log.Println("running on development configs")
	} else {
		viper.SetConfigName("production")
		log.Println("running on production configs")
	}

	viper.AddConfigPath("/apps/go/vas-dlrs/configs/")
	//viper.AddConfigPath("/Users/lukemakhanu/go/src/github.com/makhanu/vaspro-dlrs/configs/")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Println("config error should be logged ", err)
	}
	log.Println("viper initiated successfully ")
}
